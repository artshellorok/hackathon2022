<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Post;
use \App\Models\User;

class PostsController extends Controller
{
    public function store(Request $request) {
        $post = new Post();
        $post->title = $request->input('title');
        $post->desc = $request->input('desc');
        $post->user_id = $request->input('user_id');
        $image = $request->file('img');
        if ($image) {
            $original_filename_arr = explode('.', $image->getClientOriginalName());
            $file_ext = end($original_filename_arr);
            $name = md5(time() . rand()) . '.' . $file_ext;
            $dest = "./posts_img/";
            $request->file('img')->move($dest,$name);
            $post->img = "/posts_img/" . $name;
        } else 
            abort(400);
        $post->save();
        return 1;
    }

    public function view($id) {
        $user = User::find($id);
        $subscriptions = $user->subscriptions()->get();
        if (count($subscriptions)) {
            $ids = [];
            foreach ($subscriptions as $subscription)
                array_push($ids, $subscription->id);
            
            $posts = Post::whereIn('user_id', $ids)->orderBy('created_at', 'DESC')->get();

            foreach ($posts as &$post) {
                $post["comments"] = $post->comments()->get();
                $post["user"] = User::find($post->user_id);
                foreach ($post["comments"] as &$comment) {
                    $comment["user"] = $comment->user()->get()[0];
                }
            }

            return $posts;
        } else {
            $posts = Post::orderBy('created_at', 'DESC')->get();
            $posts_to_now = [];
            foreach ($posts as &$post) {
                $post["comments"] = $post->comments()->get();
                $post["user"] = User::find($post->user_id);
                foreach ($post["comments"] as &$comment) {
                    $comment["user"] = $comment->user()->get()[0];
                }
            }
            return $posts;
        }
    }

    public function comment($id, Request $request) {
        $post = Post::find($id);
        $user_id = $request->input('user_id');
        $post->comments()->create([
            "text" => $request->input('text'),
            "user_id" => $user_id,
        ]);
        return 1;
    }

}
