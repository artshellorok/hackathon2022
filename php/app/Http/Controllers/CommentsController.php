<?php

namespace App\Http\Controllers;

use \App\Models\Comment;

class CommentsController extends Controller
{
    public function delete($id) {
        return Comment::destroy($id);
    }
}
