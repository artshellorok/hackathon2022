<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use \App\Models\User;

class UserController extends Controller
{

    public function store(Request $request) {
        if($request->getMethod() === 'OPTIONS')  {
            app()->options($request->path(), function () {
                return response('OK',200)
                    ->header('Access-Control-Allow-Origin', '*')
                    ->header('Access-Control-Allow-Methods','OPTIONS, GET, POST, PUT, DELETE')
                    ->header('Access-Control-Allow-Headers', 'Content-Type, Origin');                    
            });
        }
        $user = new User();
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->name = $request->input('name');
        $avatar = $request->file('avatar');
        if ($avatar) {
            $original_filename_arr = explode('.', $avatar->getClientOriginalName());
            $file_ext = end($original_filename_arr);
            $name = md5(time() . rand()) . '.' . $file_ext;
            $dest = "./img/";
            $request->file('avatar')->move($dest,$name);
            $user->avatar = "/img/" . $name;
        } else
            $user->avatar = '/img/noimg.png';
        $user->desc = $request->input('desc');
        $user->phone = $request->input('phone');
        $user->link = $request->input('link');
        $user->save();
        return $user->id;
    }

    public function view($id) {
        return User::find($id);
    }

    public function login(Request $request) {
        $email = $request->input('email');
        $password = $request->input('password');
        $credentials = Validator::validate($request->all(), [
            'email' => ['required', 'email'],
            'password' => ['required']
        ]);
        $user = User::where('email', $request->input('email'))->first();
        if (!$user)
            return -1;
        if (Hash::check($request->input('password'), $user->password)) {
            return $user->id;
        } else
            return -2;
    }
    public function subscribe(Request $request) {
        $user_id = $request->input('user_id');
        $subscribe_id = $request->input('subscribe_id');
        $user = User::find($user_id);
        $user->subscriptions()->attach($subscribe_id);
        return 1;
    }
    public function unsubscribe(Request $request) {
        $user_id = $request->input('user_id');
        $subscribe_id = $request->input('subscribe_id');
        $user = User::find($user_id);
        $user->subscriptions()->detach($subscribe_id);
        return 1;
    }
}
