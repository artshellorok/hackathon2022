<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    protected $table = 'users';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name', 'email', 'password', 'desc', 'img', 'phone', 'link', 'avatar'
    ];

    protected $hidden = [
        'password',
    ];

    public function subscribers() {
        return $this->belongsToMany(User::class, 'subscriptions', 'user_id', 'subscried_id');
    }

    public function subscriptions() {
        return $this->belongsToMany(User::class, 'subscriptions', 'subscribed_id', 'user_id');
    }
}
