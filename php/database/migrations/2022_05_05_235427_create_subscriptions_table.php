<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->bigInteger('subscribed_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();

            $table->foreign('subscribed_id')->references('id')->on('users');
            $table->foreign('user_id')->references('id')->on('users');

            $table->primary(['user_id', 'subscribed_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
};
