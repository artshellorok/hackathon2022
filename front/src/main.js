import { createWebHistory, createRouter } from "vue-router";
import { createApp } from 'vue'
import './index.css'
import App from './App.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

import Login from './components/Login.vue'
import Register from './components/Register.vue'
import Posts from './components/Posts.vue'
import CreatePost from './components/CreatePost.vue'

const routes = [
    { path: '/', component: Login, name: "log"},
    { path: '/reg', component: Register, name: "reg"},
    { path: '/posts', component: Posts, name: "posts"},
    { path: '/posts/create', component: CreatePost, name: "posts_create"},
]

const router = createRouter({
  // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
  history: createWebHistory(),
  routes, // short for `routes: routes`
})

const app =createApp(App).use(router)
app.use(VueAxios,axios);
app.mount('#app');